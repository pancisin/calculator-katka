<?php
class FunctionObject {
    private $operands = ['+', '-', '*', '/', '**', '%'];
    private $function = "";
    private $variables = [];

    public function fromString($function) {
        $valid = false; 

        // check if string function contains operands.
        foreach($this->operands as $op) {
            if (strpos($function, $op)) $valid = true;
        }
        if (!$valid) throw new Exception('String does not contains operands.');

        //check for code injection.
        if (strpos($function, ';')) throw new Exception('There is suspicion of code injecting ! Be careful.'); 

        $this->function = strtolower($function);
        return $this;        
    }

    public function calculate() {
        foreach($this->variables as $var) {
            global $$var;
            $this->function = str_replace($var, '$'.$var, $this->function);
        }

        if (!is_array($this->variables)) throw new Exception('Have not got any variables.');
        if ($this->function == null || $this->function == "") throw new Exception('Function object was not initialized correctly !');

        $result = eval('return ' . $this->function . ';');

        if (error_get_last()) throw new Exception(error_get_last()['message']);

        return $result;
    }

    public function addVariable($key, $value) {
        global $$key;
        $$key = intval(strtolower($value));
        $this->variables[] = $key;
    }

    public function toString() {
        return $this->function;
    }

    public function getVariables() {
        $result = '';
         
        foreach($this->variables as $var) {
            global $$var;
             $result .= $var . ' = ' . $$var . ', ';
        }

        return $result;
    }
}