<!DOCTYPE html>
<html>

<head>
	<title>Calculate function</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="bootstrap.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="animate.css" type="text/css" media="all" />
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<header>
					<div class="page-header">
						<h1>Function calculation</h1>
					</div>
				</header>
				<p>
					Please enter function containing variable x to calculate.
				</p>

        <?php session_start(); ?>

        <?php if(isset($_SESSION['error'])): ?>
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Error</h3>
            </div>
            <div class="panel-body text-center">
                <?php
                  print_r($_SESSION['error']); 
                ?>
            </div>
        </div>
        <?php endif ?>

				<form method="post" action="uloha.php" id="calc_form">
					<div class="form-group">
						<label for="function">Function :</label>
						<input id="function_input" class="form-control" type="text" name="function" autocomplete="off" placeholder="(x**2+1/3*x)/2*x" required/>
					</div>

					<button class="btn btn-primary" type="submit" name="submit">Calculate</button>
				</form>

				</br>

				<?php if(isset($_SESSION['result'])): ?>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Calculation result</h3>
					</div>
					<div class="panel-body text-center">
						<p>
							<b>Input :</b>
							<?php
							print_r($_SESSION['function']); 
							?>
						</p>
						
						<p>
							<b>Result : </b>
						<?php
							print_r($_SESSION['result']); 
						?>
						</p>
					</div>
				</div>
				<?php endif; ?>

        <?php session_unset(); ?>
			</div>
		</div>
	</div>
	<script src="jquery-1.12.3.min.js"></script>
	<script>
		$("#function_input").keydown(function(e) {
			if (e.keyCode >= 65 && e.keyCode <= 90) {
				variable = String.fromCharCode(e.keyCode).toLowerCase();
				if ($( "input[name='var_" + variable + "']").length === 0) {
				
				 $("<input value='' />")
				 	.attr('name', 'var_' + variable)
					.attr('type', 'string')
					.attr('class', 'form-control')
					.insertBefore("#calc_form button")
					.wrap('<div class="form-group animated fadeInDown"></div>')
					.before('<label for="function">Value of variable ' + variable + ' : </label>')
				}
			} 
		});
	</script>
</body>

</html>