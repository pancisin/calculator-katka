<?php 
include "FunctionObject.php";

if(isset($_POST['submit']))
{
    session_start();
    try {
        $function = (new FunctionObject())->fromString($_POST["function"]);

        foreach ($_POST as $key => $value) {
            if (strpos($key, "var_") !== false) 
                $function->addVariable(str_replace('var_', '', $key), $value);
        }

        $_SESSION['function'] = $function->toString() . '; ' . $function->getVariables();
        $_SESSION['result'] = $function->calculate();
    } catch (Exception $ex) {
        $_SESSION['result'] = null;
        $_SESSION['error'] = $ex->getMessage();
    }

    header('Location: index.php');
} 